﻿namespace UmbUkFestival2014.Core.Trees
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http.Formatting;

    using umbraco;
    using umbraco.BusinessLogic.Actions;

    using Umbraco.Core;
    using Umbraco.Web.Models.Trees;
    using Umbraco.Web.Mvc;
    using Umbraco.Web.Trees;

    using UmbUkFestival2014.Core.API;

    /// <summary>
    /// The sesssion tree controller.
    /// </summary>
    [Tree(Core.Constants.UmbUkFestSection.Alias, Core.Constants.UmbUkFestSection.SessionTreeAlias, "Sessions")]
    [PluginController(Core.Constants.Routing.PluginName)]
    public class SesssionTreeController : TreeController
    {
        protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings)
        {
            // check if we're rendering the root node's children
            if (id == Constants.System.Root.ToInvariantString())
            {
                var sessionController = new SessionController();
                var nodes = new TreeNodeCollection();

                foreach (var session in sessionController.GetAll().OrderBy(x => x.Name))
                {
                    var node = this.CreateTreeNode(
                        session.Id.ToString(CultureInfo.InvariantCulture),
                        "-1",
                        queryStrings,
                        session.Name,
                        "icon-presentation",
                        false);

                    nodes.Add(node);
                }

                return nodes;
            }

            // this tree doesn't suport rendering more than 1 level
            throw new NotSupportedException();
        }

        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
        {
            var menuItemCollection = new MenuItemCollection();

            if (id == Constants.System.Root.ToInvariantString())
            {
                // root actions              
                menuItemCollection.Items.Add(
                    new MenuItem("create", ui.Text("actions", ActionNew.Instance.Alias))
                    {
                        Icon
                            =
                            "add"
                    });
                menuItemCollection.Items.Add<RefreshNode, ActionRefresh>(
                    ui.Text("actions", ActionRefresh.Instance.Alias),
                    true);
            }
            else
            {
                menuItemCollection.Items.Add<ActionDelete>(ui.Text("actions", ActionDelete.Instance.Alias));
            }

            return menuItemCollection;
        }
    }
}

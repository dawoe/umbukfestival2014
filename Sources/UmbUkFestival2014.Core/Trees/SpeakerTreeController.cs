﻿namespace UmbUkFestival2014.Core.Trees
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Net.Http.Formatting;    

    using umbraco;
    using umbraco.BusinessLogic.Actions;
    using Umbraco.Core;

    using Umbraco.Web.Models.Trees;
    using Umbraco.Web.Mvc;
    using Umbraco.Web.Trees;

    using UmbUkFestival2014.Core.API;

    /// <summary>
    /// The speaker tree controller.
    /// </summary>
    [Tree(Core.Constants.UmbUkFestSection.Alias, Core.Constants.UmbUkFestSection.SpeakerTreeAlias, "Speakers")]
    [PluginController(Core.Constants.Routing.PluginName)]
    public class SpeakerTreeController : TreeController
    {
        protected override TreeNodeCollection GetTreeNodes(string id, FormDataCollection queryStrings)
        {
            // check if we're rendering the root node's children
            if (id == Constants.System.Root.ToInvariantString())
            {
                var speakerController = new SpeakerController();
                var nodes = new TreeNodeCollection();

                foreach (var speaker in speakerController.GetAll().OrderBy(x => x.Name))
                {
                    var node = this.CreateTreeNode(
                        speaker.Id.ToString(CultureInfo.InvariantCulture),
                        "-1",
                        queryStrings,
                        speaker.Name,
                        "icon-user",
                        false);

                    nodes.Add(node);
                }

                return nodes;
            }

            // this tree doesn't suport rendering more than 1 level
            throw new NotSupportedException();
        }

        protected override MenuItemCollection GetMenuForNode(string id, FormDataCollection queryStrings)
        {
            var menuItemCollection = new MenuItemCollection();

            if (id == Constants.System.Root.ToInvariantString())
            {
                // root actions              
                menuItemCollection.Items.Add<CreateChildEntity, ActionNew>(ui.Text("actions", ActionNew.Instance.Alias));
                menuItemCollection.Items.Add<RefreshNode, ActionRefresh>(
                    ui.Text("actions", ActionRefresh.Instance.Alias),
                    true);
            }
            else
            {
                menuItemCollection.Items.Add<ActionDelete>(ui.Text("actions", ActionDelete.Instance.Alias));
            }

            return menuItemCollection;
        }
    }
}

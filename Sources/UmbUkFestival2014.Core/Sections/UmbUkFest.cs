﻿namespace UmbUkFestival2014.Core.Sections
{
    using umbraco.businesslogic;
    using umbraco.interfaces;

    /// <summary>
    /// The umb uk fest section
    /// </summary>
    [Application(Constants.UmbUkFestSection.Alias, "Umbraco UK Festival", "icon-beer-glass", 15)]
    public class UmbUkFest : IApplication
    {
    }
}

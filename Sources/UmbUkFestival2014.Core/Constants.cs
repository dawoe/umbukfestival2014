﻿namespace UmbUkFestival2014.Core
{
    /// <summary>
    /// The constants.
    /// </summary>
    public class Constants
    {
        /// <summary>
        /// The database.
        /// </summary>
        public class Database
        {
            /// <summary>
            /// The speakers table.
            /// </summary>
            public const string SpeakersTable = "Speakers";

            /// <summary>
            /// The speakers table primary key.
            /// </summary>
            public const string SpeakersTablePrimaryKey = "Id";

            /// <summary>
            /// The speakers table.
            /// </summary>
            public const string SessionTable = "Sessions";

            /// <summary>
            /// The speakers table primary key.
            /// </summary>
            public const string SessionTablePrimaryKey = "Id";
        }

        /// <summary>
        /// The routing.
        /// </summary>
        public class Routing
        {
            /// <summary>
            /// The plugin name.
            /// </summary>
            public const string PluginName = "UmbUkFest";
        }

        /// <summary>
        /// The umb uk fest section.
        /// </summary>
        public class UmbUkFestSection
        {
            /// <summary>
            /// The alias.
            /// </summary>
            public const string Alias = "umbukfest";

            /// <summary>
            /// The speaker tree alias.
            /// </summary>
            public const string SpeakerTreeAlias = "speakers";

            /// <summary>
            /// The session tree alias.
            /// </summary>
            public const string SessionTreeAlias = "sessions";
        }
    }
}

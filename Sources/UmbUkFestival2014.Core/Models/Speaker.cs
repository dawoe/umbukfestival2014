﻿namespace UmbUkFestival2014.Core.Models
{
    using Umbraco.Core.Persistence;
    using Umbraco.Core.Persistence.DatabaseAnnotations;

    /// <summary>
    /// The speaker.
    /// </summary>
    [TableName(Constants.Database.SpeakersTable)]
    [PrimaryKey(Constants.Database.SpeakersTablePrimaryKey)]
    public class Speaker
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [PrimaryKeyColumn(AutoIncrement = true)]
        [Column(Name = Constants.Database.SpeakersTablePrimaryKey)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [Length(100)]
        [NullSetting(NullSetting = NullSettings.NotNull)]
        [Column("SpeakerName")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the company.
        /// </summary>
        [Length(100)]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Company { get; set; }

        /// <summary>
        /// Gets or sets the twitter handle
        /// </summary>
        [Length(100)]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Twitter { get; set; }

        /// <summary>
        /// Gets or sets the twitter Website
        /// </summary>
        [Length(100)]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Website { get; set; }

        /// <summary>
        /// Gets or sets the image.
        /// </summary>
        [NullSetting(NullSetting = NullSettings.Null)]
        public int Image { get; set; }

        /// <summary>
        /// Gets or sets the biography.
        /// </summary>
        [SpecialDbType(SpecialDbTypes.NTEXT)]
        [NullSetting(NullSetting = NullSettings.Null)]
        public string Biography { get; set; }
    }
}

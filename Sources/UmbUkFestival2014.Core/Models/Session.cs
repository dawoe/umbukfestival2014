﻿namespace UmbUkFestival2014.Core.Models
{
    using System.Runtime.Serialization;

    using Umbraco.Core.Persistence;
    using Umbraco.Core.Persistence.DatabaseAnnotations;

    /// <summary>
    /// The session.
    /// </summary>
    [TableName(Constants.Database.SessionTable)]
    [PrimaryKey(Constants.Database.SessionTablePrimaryKey)]
    [DataContract(Name = "session")]
    public class Session
    {
        /// <summary>
        /// Gets or sets the id.
        /// </summary>
        [PrimaryKeyColumn(AutoIncrement = true)]
        [Column(Name = Constants.Database.SessionTablePrimaryKey)]
        [DataMember(Name = "id")]
        public int Id { get; set; }

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        [Length(100)]
        [NullSetting(NullSetting = NullSettings.NotNull)]
        [Column("SessionName")]
        [DataMember(Name = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the room.
        /// </summary>
        [NullSetting(NullSetting = NullSettings.NotNull)]
        [DataMember(Name = "room")]
        public string Room { get; set; }

        /// <summary>
        /// Gets or sets the time slot.
        /// </summary>
        [NullSetting(NullSetting = NullSettings.NotNull)]
        [DataMember(Name = "timeslot")]
        public string TimeSlot { get; set; }

        /// <summary>
        /// Gets or sets the speaker id.
        /// </summary>
        [NullSetting(NullSetting = NullSettings.Null)]
        [DataMember(Name = "speaker")]
        public int SpeakerId { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        [SpecialDbType(SpecialDbTypes.NTEXT)]
        [NullSetting(NullSetting = NullSettings.Null)]
        [DataMember(Name = "description")]
        public string Description { get; set; }
    }
}

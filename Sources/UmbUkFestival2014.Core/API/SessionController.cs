﻿namespace UmbUkFestival2014.Core.API
{
    using System.Collections.Generic;

    using Umbraco.Core.Persistence;
    using Umbraco.Web.Mvc;

    using UmbUkFestival2014.Core.Models;

    /// <summary>
    /// The session controller.
    /// </summary>
    [PluginController(Constants.Routing.PluginName)]
    public class SessionController : Umbraco.Web.Editors.UmbracoAuthorizedJsonController
    {
        /// <summary>
        /// Get all sessions
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{T}"/>.
        /// </returns>
        public IEnumerable<Session> GetAll()
        {
            var query = new Sql().From<Session>().OrderBy<Session>(x => x.Name);
            return DatabaseContext.Database.Fetch<Session>(query);
        }

        /// <summary>
        /// The get by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Session"/>.
        /// </returns>
        public Session GetById(int id)
        {
            var query = new Sql().From<Session>().Where<Session>(x => x.Id == id);
            return DatabaseContext.Database.FirstOrDefault<Session>(query);
        }

        /// <summary>
        /// The post save.
        /// </summary>
        /// <param name="session">
        /// The session.
        /// </param>
        /// <returns>
        /// The <see cref="Session"/>.
        /// </returns>
        public Session PostSave(Session session)
        {
            if (DatabaseContext.Database.IsNew(session))
            {
                DatabaseContext.Database.Save(session);
            }
            else
            {
                DatabaseContext.Database.Update(session);
            }

            return session;
        }

        /// <summary>
        /// The delete by id.
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        public int DeleteById(int id)
        {
            return DatabaseContext.Database.Delete<Session>(id);
        }

        /// <summary>
        /// Gets a new session object
        /// </summary>
        /// <returns>
        /// The <see cref="Session"/>.
        /// </returns>
        public Session GetNewSession()
        {
            return new Session();
        }
    }
}

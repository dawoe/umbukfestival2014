﻿namespace UmbUkFestival2014.Core.API
{
    using System.Collections.Generic;
    using System.Web.Http;

    using Umbraco.Core.Persistence;
    using Umbraco.Web.Mvc;
    using Umbraco.Web.WebApi;

    using UmbUkFestival2014.Core.Models;

    /// <summary>
    /// The speaker controller.
    /// </summary>
    [PluginController(Constants.Routing.PluginName)]
    public class SpeakerController : UmbracoAuthorizedApiController
    {
        /// <summary>
        /// Gets all speakers
        /// </summary>
        /// <returns>
        /// The <see cref="IEnumerable{T}"/>.
        /// </returns>
        [HttpGet]
        public IEnumerable<Speaker> GetAll()
        {
            var query = new Sql().From<Speaker>().OrderBy<Speaker>(x => x.Name);
            return DatabaseContext.Database.Fetch<Speaker>(query);
        }

        /// <summary>
        /// Gets a speaker by Id
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="Speaker"/>.
        /// </returns>
        [HttpGet]
        public Speaker GetById(int id)
        {
            var query = new Sql().From<Speaker>().Where<Speaker>(x => x.Id == id);
            return DatabaseContext.Database.FirstOrDefault<Speaker>(query);
        }

        /// <summary>
        /// Saves the speaker
        /// </summary>
        /// <param name="speaker">
        /// The speaker.
        /// </param>
        /// <returns>
        /// The <see cref="Speaker"/>.
        /// </returns>
        [HttpPost]
        public Speaker Save(Speaker speaker)
        {
            if (DatabaseContext.Database.IsNew(speaker))
            {
                DatabaseContext.Database.Save(speaker);
            }
            else
            {
                DatabaseContext.Database.Update(speaker);
            }

            return speaker;
        }

        /// <summary>
        /// Deletes the speaker
        /// </summary>
        /// <param name="speaker">
        /// The speaker.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        [HttpDelete]
        public int Delete(Speaker speaker)
        {
            return DatabaseContext.Database.Delete(speaker);
        }

        /// <summary>
        /// Deletes a speaker by id
        /// </summary>
        /// <param name="id">
        /// The id.
        /// </param>
        /// <returns>
        /// The <see cref="int"/>.
        /// </returns>
        [HttpDelete]
        public int DeleteById(int id)
        {
            return DatabaseContext.Database.Delete<Speaker>(id);
        }
    }
}

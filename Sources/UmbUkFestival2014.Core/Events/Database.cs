﻿namespace UmbUkFestival2014.Core.Events
{
    using Umbraco.Core;
    using Umbraco.Core.Persistence;

    /// <summary>
    /// The database events
    /// </summary>
    public class Database : ApplicationEventHandler
    {
        protected override void ApplicationStarted(UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            var db = applicationContext.DatabaseContext.Database;

            if (!db.TableExist(Core.Constants.Database.SpeakersTable))
            {
                db.CreateTable<Models.Speaker>(false);
            }

            if (!db.TableExist(Core.Constants.Database.SessionTable))
            {
                db.CreateTable<Models.Session>(false);
            }
        }
    }
}

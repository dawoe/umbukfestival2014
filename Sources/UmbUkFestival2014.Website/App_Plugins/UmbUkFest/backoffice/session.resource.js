﻿angular.module('umbraco.resources').factory('sessionResource',
    function ($http) {
        return {
            getEmpty : function() {
                return $http.get("backoffice/UmbUkFest/Session/GetNewSession");
            },
            getById: function (id) {
                return $http.get("backoffice/UmbUkFest/Session/GetById?id=" + id);
            },
            save: function (session) {
                return $http.post("backoffice/UmbUkFest/Session/PostSave", angular.toJson(session));
            },
            deleteById: function (id) {
                return $http.delete("backoffice/UmbUkFest/Session/DeleteById?id=" + id);
            }
        }
    }
);
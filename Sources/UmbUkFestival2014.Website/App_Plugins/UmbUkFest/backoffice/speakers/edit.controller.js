﻿angular.module('umbraco').controller('UmbUkFest.Speakers.EditController',
    function ($scope, $routeParams, $location, speakersResource, dialogService, entityResource, mediaHelper, notificationsService, navigationService) {
        $scope.loaded = false;
        $scope.hasimage = false;

        $scope.tabs = [{ id: "Content", label: "Speaker info" }, { id: "Logo", label: "Image" }];

        $scope.RichtTextEditor = [{
            label: 'bodyText',
            description: '',
            view: 'rte',
            config: {
                editor: {
                    toolbar: ["code", "undo", "redo", "cut", "styleselect", "bold", "italic", "alignleft", "aligncenter", "alignright", "bullist", "numlist", "link", "umbmediapicker", "umbmacro", "table", "umbembeddialog"],
                    stylesheets: [],
                    dimensions: { height: 400, width: 500 }
                }
            },
            value : ''
        }];

        if ($routeParams.id == -1) {
            $scope.speaker = {}
            $scope.loaded = true;
        }
        else {
            speakersResource.getById($routeParams.id).then(function (response) {
                $scope.speaker = response.data;
                $scope.RichtTextEditor[0].value = $scope.speaker.Biography;
                if ($scope.speaker != null && $scope.speaker.Image != null && $scope.speaker.Image != 0) {
                    entityResource.getById($scope.speaker.Image, "Media").then(function (ent) {
                        $scope.image = ent;
                        if (!ent.thumbnail) {
                            $scope.image.thumbnail = mediaHelper.resolveFileFromEntity(ent, true);
                        }                        
                    });
                }
                $scope.loaded = true;
            });
        }

        // add image function
        $scope.addImage = function() {
            dialogService.mediaPicker({
                startNodeId: -1,
                multiPicker: false,
                callback : function(data) {
                    $scope.speaker.Image = data.id;

                    $scope.image = data;                    
                }
            });
        };

        $scope.save = function() {
            if ($scope.speakerForm.$invalid) {
                return false;
            }

            if ($scope.speaker.Image == undefined || $scope.speaker.Image == null || $scope.speaker.Image == '') {
                $scope.speaker.Image = 0;
            }

            speakersResource.save($scope.speaker).then(function(response) {
                $scope.speaker = response.data;
                $scope.speakerForm.$dirty = false;
                notificationsService.success("Success", $scope.speaker.Name + " has been saved");
                var pathArray = ['-1'];
                pathArray.push($scope.speaker.Id.toString());
                navigationService.syncTree({ tree: 'speakers', path: pathArray, forceReload: true, activate: false }).then(
                    function(syncArgs) {
                        if ($routeParams.create) {
                            $location.path(syncArgs.node.routePath);
                        }
                    });
            });
        };

        $scope.RichtTextEditor = [{
            label: 'bodyText',
            description: '',
            view: 'rte',
            config: {
                editor: {
                    toolbar: ["code", "undo", "redo", "cut", "styleselect", "bold", "italic", "alignleft", "aligncenter", "alignright", "bullist", "numlist", "link", "umbmediapicker", "umbmacro", "table", "umbembeddialog"],
                    stylesheets: [],
                    dimensions: { height: 400, width: 500 }
                }
            }
        }];

        // remove image
        $scope.removeImage = function() {
            $scope.speaker.Image = 0;
            $scope.image = {};
        }

        // watch scope for changes
        $scope.$watch('speaker.Image', function() {
            $scope.hasimage = $scope.speaker != null && $scope.speaker.Image != null && $scope.speaker.Image != 0;            
        }, true);

        $scope.$watch('RichtTextEditor', function() {
            if ($scope.RichtTextEditor != undefined && $scope.loaded) {
                $scope.speaker.Biography = $scope.RichtTextEditor[0].value;
            }
        }, true);
    });
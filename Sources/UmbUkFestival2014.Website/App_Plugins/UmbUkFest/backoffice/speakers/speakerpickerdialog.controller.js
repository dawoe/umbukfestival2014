﻿angular.module("umbraco").controller("UmbUkFest.Speakers.PickerDialog.Controller",
	function ($scope) {

	    $scope.dialogEventHandler = $({});

	    $scope.dialogEventHandler.bind("treeNodeSelect", function (ev, args) {
	        args.event.preventDefault();
	        args.event.stopPropagation();
	        $scope.submit({
	            id: args.node.id,
	            name: args.node.name
	        });
	    });
	});

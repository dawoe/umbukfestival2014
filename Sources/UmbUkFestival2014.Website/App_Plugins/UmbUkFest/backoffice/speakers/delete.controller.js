﻿angular.module('umbraco').controller('UmbUkFest.Speakers.DeleteController',
    function ($scope, $location, speakersResource, navigationService, treeService, notificationsService) {
        $scope.Delete = function (id) {
           speakersResource.deleteById(id).then(function () {
                navigationService.hideNavigation();
                treeService.removeNode($scope.currentNode);
                $location.path('umbukfest');
                notificationsService.success("Success", "Speaker has been deleted");
            });
        };

        $scope.Cancel = function () {
            navigationService.hideNavigation();
        };
    });
﻿angular.module('umbraco').controller('UmbUkFest.Sessions.EditController',
    function ($scope, $routeParams, sessionResource, dialogService, notificationsService, navigationService, speakersResource, datatypeResource) {
        $scope.loaded = false;

        $scope.tabs = [{ id: "SessionInfo", label: "Session Info" }, { id: "SessionDetails", label: "Details" }];

        $scope.$watch('rteField', function () {
            if ($scope.rteField != undefined) {
                $scope.session.description = $scope.rteField[0].value;
            }
        }, true);

        sessionResource.getById($routeParams.id).then(function(response) {
            $scope.session = response.data;


            if ($scope.session.speaker != 0) {
                speakersResource.getById($scope.session.speaker).then(function(result) {
                    $scope.speakername = result.data.Name;
                });
            }

            datatypeResource.getByName('Richtext editor').then(function (result) {
                $scope.rteField = [
                   {
                       alias: 'rtefield',
                       label: 'Text',
                       view: result.data.view,
                       config: result.data.config,
                       value: $scope.session.description
                   }
                ];
            });

            $scope.loaded = true;
        });

        $scope.selectSpeaker = function() {
          dialogService.open({
                    template: "/app_plugins/umbukfest/backoffice/speakers/speakerpickerdialog.html",                   
                    callback: function(data) {
                        $scope.session.speaker = data.id;
                        $scope.speakername = data.name;
                    }
                });
        };

        $scope.removeSpeaker = function() {
            $scope.session.speaker = 0;
            $scope.speakername = '';
        };

        $scope.save = function() {
            sessionResource.save($scope.session).then(function(response) {
                $scope.session = response.data;
                notificationsService.success("Success", $scope.session.name + " has been saved");
                var pathArray = ['-1'];
                pathArray.push($scope.session.id.toString());
                navigationService.syncTree({ tree: 'sessions', path: pathArray, forceReload: true, activate: false });
            });
        };
    });
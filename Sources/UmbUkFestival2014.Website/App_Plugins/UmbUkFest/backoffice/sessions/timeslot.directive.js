﻿angular.module('umbraco.directives').directive('timeslotDropdown', function () {
    return {
        restrict: 'E',
        replace: true,
        transclude: 'true',
        template: '<select name="timeslot"><option value="09:45-10:45">09:45-10:45</option><option value="10:45-11:45">10:45-11:45</option></select>'
    };
}
);
﻿angular.module('umbraco').controller('UmbUkFest.Sessions.DeleteController',
    function ($scope, $location, sessionResource, navigationService, treeService, notificationsService) {
        $scope.Delete = function (id) {
            sessionResource.deleteById(id).then(function () {
                navigationService.hideNavigation();
                treeService.removeNode($scope.currentNode);
                $location.path('umbukfest');
                notificationsService.success("Success", "Session has been deleted");
            });
        };

        $scope.Cancel = function () {
            navigationService.hideNavigation();
        };
    });
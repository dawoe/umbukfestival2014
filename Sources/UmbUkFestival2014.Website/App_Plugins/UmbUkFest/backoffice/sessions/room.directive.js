﻿angular.module('umbraco.directives').directive('roomDropdown', function() {
    return {
        restrict: 'E',
        replace: true,
        transclude: 'true',
        template: '<select name="room" ><option value="1">Room 1</option><option value="2">Room 2</option></select>',       
        link : function(scope,elem) {
            
            }
        };
    }
);
﻿angular.module('umbraco').controller('UmbUkFest.Sessions.CreateController',
    function ($scope, $location, sessionResource, navigationService, notificationsService) {
        sessionResource.getEmpty().then(function(response) {
                $scope.session = response.data;
            }
        );

        $scope.save = function(session) {
            sessionResource.save(session).then(function(response) {
                $scope.session = response.data;
                notificationsService.success("Success", session.name + " has been created");
                var pathArray = ['-1'];
                pathArray.push($scope.session.id.toString());
                navigationService.syncTree({ tree: 'sessions', path: pathArray, forceReload: true, activate: false }).then(
                   function (syncArgs) {
                       $location.path(syncArgs.node.routePath);
                   });
                navigationService.hideNavigation();
            });
        };
    });
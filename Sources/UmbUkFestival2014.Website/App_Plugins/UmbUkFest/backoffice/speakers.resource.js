﻿angular.module('umbraco.resources').factory('speakersResource',
    function($http) {
        return {
            getById : function(id) {
                return $http.get("backoffice/UmbUkFest/Speaker/GetById?id=" + id);
            },
            save : function(speaker) {
                return $http.post("backoffice/UmbUkFest/Speaker/Save", angular.toJson(speaker));
            },
            deleteById : function(id) {
                return $http.delete("backoffice/UmbUkFest/Speaker/DeleteById?id=" + id);
            }
        }
    }
);